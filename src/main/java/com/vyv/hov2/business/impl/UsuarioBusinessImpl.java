package com.vyv.hov2.business.impl;

import com.google.common.base.Joiner;
import com.vyv.hov2.business.UsuarioBusiness;
import com.vyv.hov2.domain.Usuario;
import com.vyv.hov2.repository.UsuarioRepository;
import com.vyv.hov2.util.DataAccess.DataAccessLayerException;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.UnsupportedJwtException;
import io.jsonwebtoken.security.Keys;
import io.jsonwebtoken.security.SignatureException;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.exception.ConstraintViolationException;
import org.passay.AlphabeticalSequenceRule;
import org.passay.DigitCharacterRule;
import org.passay.LengthRule;
import org.passay.NumericalSequenceRule;
import org.passay.PasswordData;
import org.passay.PasswordValidator;
import org.passay.QwertySequenceRule;
import org.passay.RuleResult;
import org.passay.SpecialCharacterRule;
import org.passay.UppercaseCharacterRule;
import org.passay.WhitespaceRule;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Sort;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.zkoss.json.JSONValue;
import org.zkoss.zk.ui.util.Clients;

import javax.validation.ConstraintValidatorContext;
import java.io.Serializable;
import java.security.Key;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

/**
 * The type Usuario business.
 */
@Service("usuarioBusiness") public class UsuarioBusinessImpl implements UsuarioBusiness, Serializable {

    /**
     * The Usuario repository.
     */
    private final UsuarioRepository usuarioRepository;

    /**
     * The Key string.
     */
    String keyString ="M3VatsofWPmR8XiUKo9K4ZDSoAen0GOhB9tq8ASG7+E+" ;
    /**
     * The Private key bytes.
     */
    byte[] privateKeyBytes = Base64.decodeBase64(keyString);
    /**
     * The Local key.
     */
    Key localKey = Keys.hmacShaKeyFor(privateKeyBytes);
    /**
     * The Logger.
     */
    private static final Log logger = LogFactory.getLog(UsuarioBusinessImpl.class);

    /**
     * Instantiates a new Usuario business.
     *
     * @param usuarioRepository            the usuario repository
     */
    public UsuarioBusinessImpl(UsuarioRepository usuarioRepository) {
        this.usuarioRepository = usuarioRepository;

    }


    @Override
    @Transactional
    public Usuario save(Usuario usuario) throws DataAccessLayerException {
        try {
            usuario = usuarioRepository.save(usuario);
            return usuario;
        } catch (ConstraintViolationException ex) {
            String constraintName;
            String mensajeException = "Error Guardando Usuario c.v.e.";
            constraintName = ex.getConstraintName();

            this.logger.error("Usuario Error Constraint");
            this.logger.error("Mensaje: " + ex.getMessage());
            this.logger.error("Sql: " + ex.getSQL());
            this.logger.error("Estado: " + ex.getSQLState());
            this.logger.error("Nombre Constraint - " + constraintName);
            this.logger.error(ex.getSQLException());

            if ("usuario_login_key".equals(constraintName)) {
                mensajeException += " - El Login ya se encuentra registrado";
            }

            throw new DataAccessLayerException(mensajeException,ex);

        } catch (DataIntegrityViolationException ex) {
            this.logger.error("Error Guardando Usuario d.i.v.");
            this.logger.error("mensaje: " + ex.getMessage());
            String constraintName;
            String mensajeException = "Error Guardando Usuario";
            constraintName = ex.getMessage();
            if ((constraintName.contains("uc_usuarioemail_col"))) {
                mensajeException += " - Este email ya se encuentra registrado";
            }
            throw new DataAccessLayerException(mensajeException, ex);
        } catch ( Exception ex) {
            this.logger.error("Error Guardando Usuario  e.");
            this.logger.error("mensaje: " + ex.getMessage());
            this.logger.error("class: " + ex.getClass());
            throw new DataAccessLayerException("Error Guardando Usuario", ex);
        }
    }

    @Override
    @Transactional
    public void delete(Usuario usuario) throws DataAccessLayerException {
        try {
            usuarioRepository.delete(usuario);
        } catch (ConstraintViolationException ex) {
            String constraintName;
            String mensajeException = "Error Eliminando Usuario";
            constraintName = ex.getConstraintName();

            this.logger.error("Eliminando Usuario Error Constraint");
            this.logger.error("Mensaje: " + ex.getMessage());
            this.logger.error("Sql: " + ex.getSQL());
            this.logger.error("Estado: " + ex.getSQLState());
            this.logger.error("Nombre Constraint - " + constraintName);
            this.logger.error(ex.getSQLException());

            throw new DataAccessLayerException(mensajeException,ex);

        } catch (DataIntegrityViolationException ex) {
            this.logger.error("Error Eliminando Usuario");
            this.logger.error("mensaje: " + ex.getMessage());
            throw new DataAccessLayerException("Error Eliminando Usuario");
        }
    }
    @Override
    @Transactional
    public Usuario get(Long id) {
        return this.usuarioRepository.findOne(id);
    }
    @Override
    @Transactional
    public List<Usuario> get() throws DataAccessLayerException {
        try {
            return (List<Usuario>) this.usuarioRepository.findAll(Sort.by(Sort.Direction.ASC, "nombre"));
        } catch (DataIntegrityViolationException ex) {
            this.logger.error("Error Listando Usuarios");
            this.logger.error("mensaje: " + ex.getMessage());
            throw new DataAccessLayerException("Error Listando Usuarios");
        }
    }

    @Override
    @Transactional
    public List<Usuario> getAppUsuarios() throws DataAccessLayerException {
        try {
            return this.usuarioRepository.findAllByRoldbOrderByNombreAsc("invitado");
        } catch (DataIntegrityViolationException ex) {
            this.logger.error("Error Listando Usuarios de app");
            this.logger.error("mensaje: " + ex.getMessage());
            throw new DataAccessLayerException("Error Listando Usuarios");
        }
    }


    @Override
    @Transactional
    public Usuario validar(String login, String password, String rol) throws DataAccessLayerException {
        try {
            return null;
            //this.usuarioRepository.validar(login, password, rol);
        } catch (DataIntegrityViolationException ex) {
            this.logger.error("Error Validando Usuario");
            this.logger.error("mensaje: " + ex.getMessage());
            this.logger.error("ex to string: " + ex.toString());
            throw new DataAccessLayerException("Sin coneccion a la base de datos !");
        }

    }

    @Override
    public boolean passwordIsValid(final String password, final ConstraintValidatorContext context) {
        // @formatter:off
        final PasswordValidator validator = new PasswordValidator(
                Arrays.asList(new LengthRule(8, 30), new UppercaseCharacterRule(1), new DigitCharacterRule(1), new SpecialCharacterRule(1),
                        new NumericalSequenceRule(3, false), new AlphabeticalSequenceRule(3, false), new QwertySequenceRule(3, false),
                        new WhitespaceRule()));
        final RuleResult result = validator.validate(new PasswordData(password));
        if (result.isValid()) {
            return true;
        }
        context.disableDefaultConstraintViolation();
        context.buildConstraintViolationWithTemplate(Joiner.on(",").join(validator.getMessages(result))).addConstraintViolation();
        Clients.showNotification("Clave Invalida :" + context.getDefaultConstraintMessageTemplate(), "info", null, "end_center", 3000);
        return false;
    }

    @Override
    public void deletePasswordResetTokenForUser(String myToken) {

    }


    @Override
    @Transactional
    public Usuario findUserByEmail(String email) {
        try {
            return this.usuarioRepository.findFirstByEmailIgnoreCase(email);
        } catch (DataIntegrityViolationException ex) {
            this.logger.error("Error buscando usuario por email");
            this.logger.error("mensaje: " + ex.getMessage());
            this.logger.error("ex to string: " + ex.toString());
            throw new DataAccessLayerException("error");
        }
    }


    @Override
    @Transactional
    public List<Usuario> buscar(Usuario usuario) throws DataAccessLayerException {
        if ((usuario.getLogin() != null && !usuario.getLogin().isEmpty()) && (usuario.getRoldb() == null || usuario.getRoldb().isEmpty())) {
            return this.usuarioRepository.findByLoginContainingIgnoreCase(usuario.getLogin());
        }

        if ((usuario.getLogin() == null || usuario.getLogin().isEmpty()) && usuario.getRoldb() != null && !usuario.getRoldb().isEmpty()) {
            return usuarioRepository.findAllByRoldbOrderByNombreAsc(usuario.getRoldb());
        }

        if ((usuario.getLogin() != null && !usuario.getLogin().isEmpty()) && usuario.getRoldb() != null && !usuario.getRoldb().isEmpty()) {
            return this.usuarioRepository.findByLoginContainingIgnoreCaseAndRoldb(usuario.getLogin(), usuario.getRoldb());
        }

        return (List<Usuario>) usuarioRepository.findAll();
    }

    @Override
    @Transactional
    public Usuario buscarId(Long id) throws DataAccessLayerException {
        try {
            return usuarioRepository.findOne(id);
        } catch (DataIntegrityViolationException ex) {
            this.logger.error("Error Buscando Usuario Por ID");
            this.logger.error("mensaje: " + ex.getMessage());
            throw new DataAccessLayerException("Error Buscando Usuario Por ID");
        }

    }

    @Override
    @Transactional
    public Boolean hasRole(String role) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication.getAuthorities() != null) {
            return authentication.getAuthorities().stream().anyMatch(r -> r.getAuthority().equals(role));
        } else {
            return false;
        }
    }


    @Override
    @Transactional
    public void changeUserPassword(final Usuario user, final String password) {
        user.setPassword(BCrypt.hashpw(password, BCrypt.gensalt(12)));
        this.save(user);
    }

    @Override
    public void createPasswordResetTokenForUser(Usuario user, String token) {

    }

    @Override
    public String validatePasswordResetToken(long id, String token) {
        return null;
    }


    @Override
    @Transactional
    public Usuario checkTokenFromJson(String json,String authorization, String idUsuario){
        // eso es feo pero es necesario para tener compatibilidad entre las varias formas de enviar
        // los credenciales entre varias versiones de la app, de las API externa o de la pagina web...
        Map obj=(Map) JSONValue.parse(json);
        String login = obj.get("login")!= null? (String)obj.get("login"):null;
        String token = obj.get("token")!= null? (String)obj.get("token"):null;
        if(authorization==null && obj.get("authorization")!= null) {
            authorization = ((String)obj.get("authorization").toString().replace("Bearer ","") );
        }
        if (authorization!=null)  token=authorization.replace("Bearer ","");
        if(token== null) return null;
        Long id = null;
        if (idUsuario!= null) {
            try {
                id = Long.parseLong(idUsuario);
            } catch (Exception ex){
                id = null;
            }
        }
        logger.debug("checking for login/ idUsuario / token : " + login + "/" + idUsuario + "/" + token);
        Usuario log =this.checkAutentication(login, id, token);
        return log;
    }

    @Override
    public Key getLocalKey() {
        return localKey;
    }


    @Override
    @Transactional
    public Usuario login(String login){
        return this.usuarioRepository.findFirstByLoginIgnoreCase(login);
    }
    @Override
    @Transactional
    public Usuario email(String email){
        return this.usuarioRepository.findFirstByEmailIgnoreCase(email);
    }




    /**
     * Token is valid boolean.
     *
     * @param subject the subject
     * @param token   the token
     * @return the boolean
     */
    private Boolean tokenIsValid  (String subject, String  token){
        try{

            Boolean response = Jwts.parserBuilder()
                    .setSigningKey(localKey).build().parseClaimsJws(token).getBody().getSubject().equals(subject);
            logger.debug("response: " + response);
            return response;
        } catch (ExpiredJwtException | UnsupportedJwtException | MalformedJwtException | SignatureException | IllegalArgumentException e) {
            logger.error("error: " + e.getMessage());
            return false;
        }
    }

    @Override
    @Transactional
    public Usuario checkAutentication (String login , Long usuarioID, String jws ) {
        Usuario user = null;
        Boolean jwtIsValid = false;
        if (usuarioID!=null) {
            user =this.get(usuarioID);
        } else {
            if (login != null) {
                user = this.login(login);
            }
        }
        if (user!= null) {
            if (login== null) {
                try {
                    // este try catch es necesario porque aunque no este nulo user el objeto creado con
                    // findOne de JPA puede no existir en base de datos y cuando uno va a buscar el login ahora si lo va y lo busca en BD
                    // si no lo encuentra entonce no se valida la autenticación
                    login = user.getLogin();
                } catch ( Exception ex) {
                    ex.printStackTrace();
                    return null;
                }
            }
            logger.debug("login: " + login);
            logger.debug("jws: " + jws);
            jwtIsValid = this.tokenIsValid(login, jws);
            logger.debug("jwtIsValid 1: " + jwtIsValid);
            if (jwtIsValid) {
                logger.debug("Validation by jws OK");
                return user;
            } else {
                // eso porque con el login por defecto de la pagina web toca averiguar si con el login del user si pasa...
                login = user.getLogin();
                logger.debug("login: " + login);
                logger.debug("jws: " + jws);
                jwtIsValid = this.tokenIsValid(login, jws);
                if (jwtIsValid) {
                    logger.debug("Validation by jws OK");
                    return user;
                }
                logger.error("jws expired or forged ! login " + login);
                return null;
            }
        } else {
            logger.info("usuario no valido o no encontrado !" + login + " " + usuarioID);
            return null;
        }
    }
}
