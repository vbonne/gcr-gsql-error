package com.vyv.hov2.business;

import com.vyv.hov2.domain.Usuario;
import com.vyv.hov2.util.DataAccess.DataAccessLayerException;

import javax.validation.ConstraintValidatorContext;
import java.security.Key;
import java.util.List;

/**
 * The interface Usuario business.
 */
public interface UsuarioBusiness {

    /**
     * Save usuario.
     *
     * @param usuario the usuario
     * @return the usuario
     * @throws DataAccessLayerException the data access layer exception
     */
    Usuario save(Usuario usuario) throws DataAccessLayerException;

    /**
     * Delete.
     *
     * @param usuario the usuario
     * @throws DataAccessLayerException the data access layer exception
     */
    void delete(Usuario usuario) throws DataAccessLayerException;

    /**
     * Get list.
     *
     * @return the list
     * @throws DataAccessLayerException the data access layer exception
     */
    List<Usuario> get() throws DataAccessLayerException;

    /**
     * Buscar list.
     *
     * @param usuario the usuario
     * @return the list
     * @throws DataAccessLayerException the data access layer exception
     */
    List<Usuario> buscar(Usuario usuario) throws DataAccessLayerException;

    /**
     * Buscar id usuario.
     *
     * @param id the id
     * @return the usuario
     * @throws DataAccessLayerException the data access layer exception
     */
    Usuario buscarId(Long id) throws DataAccessLayerException;

    /**
     * Validar usuario.
     *
     * @param login    the login
     * @param password the password
     * @param rol      the rol
     * @return the usuario
     * @throws DataAccessLayerException the data access layer exception
     */
    Usuario validar(String login, String password, String rol) throws DataAccessLayerException;

    /**
     * Has role boolean.
     *
     * @param role the role
     * @return the boolean
     */
    Boolean hasRole(String role);

    /**
     * Gets app usuarios.
     *
     * @return the app usuarios
     * @throws DataAccessLayerException the data access layer exception
     */
    List<Usuario> getAppUsuarios() throws DataAccessLayerException;

    /**
     * Find user by email usuario.
     *
     * @param email the email
     * @return the usuario
     * @throws DataAccessLayerException the data access layer exception
     */
    Usuario findUserByEmail(String email) throws DataAccessLayerException;

    /**
     * Change user password.
     *
     * @param user     the user
     * @param password the password
     */
    void changeUserPassword(Usuario user, String password);

    /**
     * Create password reset token for user.
     *
     * @param user  the user
     * @param token the token
     */
    void createPasswordResetTokenForUser(Usuario user, String token);

    /**
     * Validate password reset token string.
     *
     * @param id    the id
     * @param token the token
     * @return the string
     */
    String validatePasswordResetToken(long id, String token);

    /**
     * Password is valid boolean.
     *
     * @param password the password
     * @param context  the context
     * @return the boolean
     */
    boolean passwordIsValid( String password,  ConstraintValidatorContext context);

    /**
     * Delete password reset token for user.
     *
     * @param myToken the my token
     */
    void deletePasswordResetTokenForUser(final String myToken);
    /**
     * Check token from json usuario.
     *
     * @param json          the json
     * @param authorization the authorization
     * @return the usuario
     */
    Usuario checkTokenFromJson(String json, String authorization, String idUsuario);
    /**
     * Check autentication usuario.
     *
     * @param login     the login
     * @param usuarioID the usuario id
     * @param jws       the jws
     * @return the usuario
     */
    Usuario checkAutentication (String login , Long usuarioID, String jws );
    /**
     * Gets local key.
     *
     * @return the local key
     */
    Key getLocalKey();
    /**
     * find by Login usuario.
     *
     * @param login the login
     * @return the usuario
     */
    Usuario login(String login);
    /**
     * find by email usuario.
     *
     * @param login the login
     * @return the usuario
     */
    Usuario email(String login);

    /**
     * Get usuario.
     *
     * @param id the id
     * @return the usuario
     */
    Usuario get(Long id);

}
