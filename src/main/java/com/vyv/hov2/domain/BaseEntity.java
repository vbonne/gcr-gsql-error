package com.vyv.hov2.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Hibernate;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.proxy.HibernateProxy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * The type Base entity.
 */
@MappedSuperclass public class BaseEntity implements Serializable {

    /**
     * The constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The Id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * The Created date.
     */
    @Basic(optional = false)
    @Column(columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP", name = "createddate")
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    @CreationTimestamp
    @JsonIgnore
    private Date createdDate;

    /**
     * The Updated date.
     */
    @Basic(optional = false)
    @Column(columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP", name = "updateddate")
    @Temporal(TemporalType.TIMESTAMP)
    @UpdateTimestamp
    @LastModifiedDate
    @JsonIgnore
    private Date updatedDate;

    /**
     * The Version.
     */
    @Version
    @Column(columnDefinition = "INT DEFAULT 1")
    @JsonIgnore
    private Integer version;

    /**
     * The constant logger.
     */
    @Transient
    static Log logger = LogFactory.getLog(BaseEntity.class);

    /**
     * Instantiates a new Base entity.
     */
    public BaseEntity() {
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(Long id) {
        this.id = id;
    }


    /**
     * Gets version.
     *
     * @return the version
     */
    public Integer getVersion() {
        return version;
    }

    /**
     * Sets version.
     *
     * @param version the version
     */
    public void setVersion(Integer version) {
        this.version = version;
    }

    /**
     * Gets created date.
     *
     * @return the created date
     */
    public Date getCreatedDate() {
        return createdDate;
    }

    /**
     * Sets created date.
     *
     * @param createdDate the created date
     */
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    /**
     * Gets updated date.
     *
     * @return the updated date
     */
    public Date getUpdatedDate() {
        return updatedDate;
    }

    /**
     * Sets updated date.
     *
     * @param updatedDate the updated date
     */
    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }


    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */


    // intente quitar este override pero sin el ZK nunca logra comparar elementos de una lista

    @Override
    public int hashCode() {
        return Objects.hash(id, version);
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */

    // intente quitar este overrides pero sin el ZK nunca logra comparar elementos de una lista
    @Override
    public boolean equals(Object obj) {
        Boolean pleaseLog = false;
        if (obj != null) {
            if (obj.getClass().getSimpleName().toUpperCase().contains("ASDADFS")) {
                pleaseLog = true;
                logger.info("this.id " + this.getId());
                logger.info("obj.id " + ((BaseEntity) obj).getId());
            }
        }
        try {
            if (this == obj) {
                if (pleaseLog) logger.info("se dice si son iguales porque los ojetos son los mismos");
                return true;
            }
            if (obj == null) {
                if (pleaseLog) logger.info("se dice que no iguales porque el otro es nulo");
                return false;
            }
        } catch (Exception ex) {
            logger.error("Error en un .equals comparando objetos basicos " + ex.getMessage());
            ex.printStackTrace();
            return false;
        }
        try {
            if (getClassForHibernateObject(this) == null || getClassForHibernateObject(obj) == null || !(getClassForHibernateObject(this)
                                                                                                                 .equals(getClassForHibernateObject(
                                                                                                                         obj)))) {
                if (pleaseLog) logger.info("se dice que no iguales porque no son el mismo tipo");
                return false;
            }
        } catch (Exception ex) {
            logger.error("Error en un .equals comparando classes " + ex.getMessage());
            ex.printStackTrace();
            return false;
        }
        try {
            BaseEntity other = (BaseEntity) obj;
            if (this.getId() == null) {
                if (other.getId() != null) {
                    if (pleaseLog) {
                        logger.info("se dice que no iguales porque el primero no tiene id pero el segundo si");
                    }
                    return false;
                } else {

                    if (this.getVersion() != null && !this.getVersion().equals(other.getVersion())) {
                        if (pleaseLog) {
                            logger.info("aca los 2 ids son nullos, comparando con las fechas de creaciones y actulization se determino "
                                        + "que son distintos");
                        }
                        return false;
                    }
                }
            } else if (!this.getId().equals(other.getId()) ||  // aca toca usar los getXXX el aceso directo a la propriedad sobre obj
                       // siempre devuelve nullo !!
                       (this.getVersion() != null && !this.getVersion().equals(other.getVersion()))) {
                if (pleaseLog) {
                    logger.info("se dice que no iguales porque no tienen el mismo id /version / created Date o UpdatedDate");
                }
                if (pleaseLog) logger.info("id 1 " + id);
                if (pleaseLog) logger.info("id 2 " + other.getId());
                return false;
            }
            if (pleaseLog) {
                logger.info("se dice si son iguales porque no se pudo indentificar differencias," + " pero toca implementar un igual a "
                            + "nivel superior si los queremos comparar mas precisamente ");
            }
            return true;
        } catch (Exception ex) {
            logger.error("Error en un .equals comparando datos " + ex.getMessage());
            ex.printStackTrace();
            return false;
        }
    }

    /**
     * Gets class for hibernate object.
     *
     * @param <T>    the type parameter
     * @param entity the entity
     * @return the class for hibernate object
     */
    public static <T> Class<?> getClassForHibernateObject(T entity) {
        if (entity instanceof HibernateProxy) {
            try {
                return (Hibernate.getClass(entity));
            } catch (Exception ex) {
                logger.error("ERROR en Hibernate.getClass  en getClassForHibernateObject() " + ex.getMessage());
                ex.printStackTrace();
                return null;
            }
        } else {
            return entity.getClass();
        }
    }


}
