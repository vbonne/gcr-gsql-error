package com.vyv.hov2.domain;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Collection;

/**
 * The type Usuario.
 *
 * @author Vincent Bonnefon <vincent@bonnefon.com>
 */
@Entity @Table(name = "usuario", schema = "public", uniqueConstraints = {}) public class Usuario extends BaseEntity implements Cloneable {

    /**
     * The Login.
     */
    @Basic(optional = false)
    @Column(unique = true, name = "login", nullable = false, length = 45)
    private String login;
    /**
     * The Password.
     */
    @Basic(optional = true)
    @Column(name = "password", nullable = true, length = 45)
    private String password;
    /**
     * The Roldb.
     */
    @Basic(optional = false)
    @Column(name = "roldb", nullable = false, length = 30)
    private String roldb;
    /**
     * The Activo.
     */
    @Basic(optional = false)
    @Column(name = "activo", nullable = false,columnDefinition = "boolean default true")
    private boolean activo;
    /**
     * The Nombre.
     */
    @Basic(optional = true)
    @Column(name = "nombre", nullable = true, length = 50)
    private String nombre;
    /**
     * The Email.
     */
    @Basic(optional = true)
    @Column(unique = true, name = "email", nullable = true, length = 100)
    private String email;

    /**
     * The Ext sip.
     */
    @Basic(optional = true)
    @Column(name = "ext_sip")
    private String extSip;
    /**
     * The Es externo.
     */
    @Basic(optional = true)
    @Column(name = "is_affiliate")
    private Boolean esExterno;

    /**
     * The Theme.
     */
    @Basic(optional = true)
    @Column(name = "theme")
    private String theme;
    /**
     * The Celular.
     */
    @Basic(optional = true)
    @Column(name = "celular")
    private String celular;
    /**
     * The Default printer.
     */
    @Basic(optional = true)
    @Column(name = "defaultprinter")
    private String defaultPrinter;


    /**
     * The Activo.
     */
    @Basic(optional = true)
    @Column(name = "is_travel_arranger", nullable = true,columnDefinition = "boolean default false")
    private boolean isTravelArranger;

    /**
     * The Activo.
     */
    @Basic(optional = true)
    @Column(name = "cannot_see_rate", nullable = true,columnDefinition = "boolean default false")
    private boolean cannotSeeRate;

    /**
     * The Activo.
     */
    @Basic(optional = true)
    @Column(name = "cannot_see_discount", nullable = true,columnDefinition = "boolean default false")
    private boolean cannotSeeDiscount;

    /**
     * The Activo.
     */
    @Basic(optional = true)
    @Column(name = "is_admin", nullable = true,columnDefinition = "boolean default false")
    private boolean isAdministrator;



    /**
     * Gets es externo.
     *
     * @return the es externo
     */
    public Boolean getEsExterno() {
        return esExterno;
    }

    /**
     * Sets es externo.
     *
     * @param esExterno the es externo
     */
    public void setEsExterno(Boolean esExterno) {
        this.esExterno = esExterno;
    }


    /**
     * Gets nombre.
     *
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Sets nombre.
     *
     * @param nombre the nombre
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Gets login.
     *
     * @return the login
     */
    public String getLogin() {
        return login;
    }

    /**
     * Is activo boolean.
     *
     * @return the boolean
     */
    public boolean isActivo() {
        return activo;
    }

    /**
     * Sets activo.
     *
     * @param activo the activo
     */
    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    /**
     * Sets activo.
     *
     * @param activo the activo
     */
    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    /**
     * Sets login.
     *
     * @param login the login
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     * Gets password.
     *
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets password.
     *
     * @param password the password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Gets roldb.
     *
     * @return the roldb
     */
    public String getRoldb() {
        return roldb;
    }

    /**
     * Sets roldb.
     *
     * @param roldb the roldb
     */
    public void setRoldb(String roldb) {
        this.roldb = roldb;
    }

    /**
     * Instantiates a new Usuario.
     */
    public Usuario() {
        this.login = "";
        this.roldb = "";
        this.activo = true;
    }


    /**
     * Gets email.
     *
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets email.
     *
     * @param email the email
     */
    public void setEmail(String email) {
        this.email = email != null ? email.trim():null;
    }

    /**
     * Gets ext sip.
     *
     * @return the ext sip
     */
    public String getExtSip() {
        return extSip;
    }

    /**
     * Sets ext sip.
     *
     * @param extSip the ext sip
     */
    public void setExtSip(String extSip) {
        this.extSip = extSip;
    }




    /**
     * Gets theme.
     *
     * @return the theme
     */
    public String getTheme() {
        return theme;
    }

    /**
     * Sets theme.
     *
     * @param theme the theme
     */
    public void setTheme(String theme) {
        this.theme = theme;
    }

    /**
     * Gets celular.
     *
     * @return the celular
     */
    public String getCelular() {
        return celular;
    }

    /**
     * Sets celular.
     *
     * @param celular the celular
     */
    public void setCelular(String celular) {
        this.celular = celular;
    }

    /**
     * Gets default printer.
     *
     * @return the default printer
     */
    public String getDefaultPrinter() {
        return defaultPrinter;
    }

    /**
     * Sets default printer.
     *
     * @param defaultPrinter the default printer
     */
    public void setDefaultPrinter(String defaultPrinter) {
        this.defaultPrinter = defaultPrinter;
    }


    public boolean getIsTravelArranger() {
        return isTravelArranger;
    }
    public void setIsTravelArranger(boolean travelArranger) {
        isTravelArranger = travelArranger;
    }

    public boolean getIsAdministrator() {
        return isAdministrator;
    }

    public void setIsAdministrator(boolean administrator) {
        isAdministrator = administrator;
    }

    public boolean isTravelArranger() {
        return isTravelArranger;
    }

    public void setTravelArranger(boolean travelArranger) {
        isTravelArranger = travelArranger;
    }

    public boolean isCannotSeeRate() {
        return cannotSeeRate;
    }

    public void setCannotSeeRate(boolean cannotSeeRate) {
        this.cannotSeeRate = cannotSeeRate;
    }

    public boolean isCannotSeeDiscount() {
        return cannotSeeDiscount;
    }

    public void setCannotSeeDiscount(boolean cannotSeeDiscount) {
        this.cannotSeeDiscount = cannotSeeDiscount;
    }

    public boolean isAdministrator() {
        return isAdministrator;
    }

    public void setAdministrator(boolean administrator) {
        isAdministrator = administrator;
    }
}
