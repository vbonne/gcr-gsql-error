package com.vyv.hov2.web;

import org.zkoss.zk.ui.http.HttpSessionListener;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;

/**
 * The type Http session pool.
 *
 * @author Maikel Chandika <mkdika@gmail.com>
 */
public class HttpSessionPool extends HttpSessionListener {

    @Override
    public void sessionCreated(HttpSessionEvent evt) {
        super.sessionCreated(evt);
        HttpSession session = evt.getSession();
    }


    @Override
    public void sessionDestroyed(HttpSessionEvent evt) {
        super.sessionDestroyed(evt);
        HttpSession session = evt.getSession();
    }
}
