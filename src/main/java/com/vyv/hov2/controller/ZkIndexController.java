package com.vyv.hov2.controller;

import com.vyv.hov2.domain.Usuario;
import com.vyv.hov2.security.AppUserPrincipal;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

/**
 * The type Index controller.
 *
 * @author Vincent Bonnefon <vincent@bonnefon.com>
 */
@Controller public class ZkIndexController extends ZkBaseControler implements ErrorController {
    private static final Log logger = LogFactory.getLog(ZkBaseControler.class);
    /**
     * Crear string.
     *
     * @return the string
     */
    @RequestMapping(value = {"", "/", "index", "/index.html"}, method = RequestMethod.GET)
    public String crear() {
        //aca viene la logica de todo lo que tiene que cargar al inicio

        return "alertas.zul";
    }

    /**
     * Login string.
     *
     * @return the string
     */
    @RequestMapping(value = {"/login.html", "login.zul", "login", "/login", "/login.zul"}, method = RequestMethod.GET)
    public String login(HttpServletRequest request) {
        //Interfaz De login
        AppUserPrincipal appUser = null;
        try {
            appUser = (AppUserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        } catch (Exception ex){
            logger.debug("error determinando usuario logeado, probablemente nadie esta logeado ");
        }
        Usuario usuarioLogged = appUser!= null ? appUser.getUsuario():null;
        logger.debug("appUser " + appUser);
        logger.debug("usuarioLogged " + usuarioLogged);
        if (usuarioLogged != null && usuarioLogged.getRoldb() != null
            && !"invitado".equalsIgnoreCase(usuarioLogged.getRoldb()) ) {
            logger.debug("hay un usuario, pasamos la etapa del login");
            return "alertas.zul";
        } else {
            logger.debug("no hay un usuario autorizado, mandamos al login");
        }
        return "login.zul";
    }

    /**
     * Inicio string.
     *
     * @return the string
     */
    @RequestMapping(value = {"inicio"}, method = RequestMethod.GET)
    public String inicio() {
        //aca viene la logica de todo lo que tiene que cargar cuando el usuario hace click en home
        return "alertas.zul";
    }

    /**
     * Error string.
     *
     * @param request the request
     * @param model   the model
     * @return the string
     */
    @RequestMapping(value = {"/error"}, method = RequestMethod.GET)
    public String error(HttpServletRequest request, ModelMap model) {
        model.addAttribute("status", request.getAttribute("javax.servlet.error.status_code"));
        model.addAttribute("reason", request.getAttribute("javax.servlet.error.message"));
        return "error.zul";
    }

    /**
     * About us string.
     *
     * @return the string
     */
    @RequestMapping(value = {"/aboutus"}, method = RequestMethod.GET)
    public String aboutUs() {
        //Interfaz De login
        return "aboutus.zul";
    }


}
