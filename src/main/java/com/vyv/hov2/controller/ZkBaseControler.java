package com.vyv.hov2.controller;

import com.vyv.hov2.security.AppUserPrincipal;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.servlet.http.HttpServletRequest;

/**
 * The type Base controler.
 */
public class ZkBaseControler {

    /**
     * Gets user.
     *
     * @param request the request
     * @return the user
     */
    @ModelAttribute("user")
    public AppUserPrincipal getUser(HttpServletRequest request) {
        return (AppUserPrincipal) request.getAttribute("user");
    }


}
