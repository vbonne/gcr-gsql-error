package com.vyv.hov2.controller;

import com.vyv.hov2.business.UsuarioBusiness;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;


/**
 * The type User controller.
 *
 * @author Felix Orduz
 */
@Controller @RequestMapping("/user") public class ZkUserController {
    /**
     * The Usuario b.
     */
    @Autowired
    UsuarioBusiness usuarioB;
    /**
     * The Logger.
     */
    private static final Log logger = LogFactory.getLog(ZkUserController.class);

    /**
     * Profile string.
     *
     * @param model the model
     * @return the string
     */
    @RequestMapping(value = "/profile", method = RequestMethod.GET)
    public String profile(ModelMap model) {
        return "/usuario/perfil.zul";
    }

    /**
     * Reset password string.
     *
     * @param request       the request
     * @param model         the model
     * @param fromMobileApp the from mobile app
     * @return the string
     */
    @RequestMapping(value = "/resetPassword", method = RequestMethod.GET)
    public String resetPassword(HttpServletRequest request, ModelMap model,
                                @RequestParam(value = "fromMobileApp", required = false) Boolean fromMobileApp) {
        logger.info("Entrando en el dispatcher para resetear password");
        model.addAttribute("type", "reset");
        model.addAttribute("appUrl", this.getAppUrl(request));
        model.addAttribute("message", "");
        model.addAttribute("id", 0L);
        model.addAttribute("token", "");
        model.addAttribute("fromMobileApp", fromMobileApp);
        return "/changepassword.zul";
    }

    /**
     * Show change password page string.
     *
     * @param locale        the locale
     * @param model         the model
     * @param id            the id
     * @param token         the token
     * @param fromMobileApp the from mobile app
     * @return the string
     */
    @RequestMapping(value = "/changePassword", method = RequestMethod.GET)
    public String showChangePasswordPage(Locale locale, ModelMap model, @RequestParam("id") long id, @RequestParam("token") String token,
                                         @RequestParam(value = "fromMobileApp", required = false) Boolean fromMobileApp) {
        String result = usuarioB.validatePasswordResetToken(id, token);
        if (result != null) {
            model.addAttribute("type", "change");
            model.addAttribute("appUrl", "");
            model.addAttribute("message", result);
            model.addAttribute("id", 0L);
            model.addAttribute("token", token);
            model.addAttribute("fromMobileApp", fromMobileApp);
            return "redirect:/login";
        } else {
            model.addAttribute("type", "change");
            model.addAttribute("id", id);
            model.addAttribute("token", token);
            return "/changepassword.zul";
        }
    }

    /**
     * Show update password page string.
     *
     * @param locale the locale
     * @param model  the model
     * @return the string
     */
    @RequestMapping(value = "/updatePassword", method = RequestMethod.GET)
    public String showUpdatePasswordPage(Locale locale, ModelMap model) {
        model.addAttribute("type", "update");
        model.addAttribute("appUrl", "");
        model.addAttribute("message", "");
        model.addAttribute("id", 0L);
        model.addAttribute("token", "");
        return "/changepassword.zul";

    }

    /**
     * Gets app url.
     *
     * @param request the request
     * @return the app url
     */
    private String getAppUrl(HttpServletRequest request) {
        return "https://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
    }

}
