package com.vyv.hov2.repository;


import com.vyv.hov2.domain.BaseEntity;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;

/**
 * The interface Custom repository.
 *
 * @param <T>  the type parameter
 * @param <ID> the type parameter
 */
@NoRepositoryBean interface CustomRepository<T extends BaseEntity, ID extends Serializable> extends Repository<T, ID> {
    /**
     * Save s.
     *
     * @param <S>    the type parameter
     * @param entity the entity
     * @return the s
     */
    @Transactional
    <S extends T> S save(S entity);

    /**
     * Find one t.
     *
     * @param primaryKey the primary key
     * @return the t
     */
    @Transactional
    T findOne(ID primaryKey);


    /**
     * Find all iterable.
     *
     * @return the iterable
     */
    @Transactional
    List<T> findAll();

    /**
     * Delete.
     *
     * @param entity the entity
     */
    @Transactional
    void delete(T entity);

    /**
     * Find all iterable.
     *
     * @param sort the sort
     * @return the iterable
     */
    @Transactional
    List<T> findAll(Sort sort);

    /**
     * Detach.
     *
     * @param klass the klass
     */
    void detach(T klass);

    /**
     * Flush.
     */
    void flush();
}
