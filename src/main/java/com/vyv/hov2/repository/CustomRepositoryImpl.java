package com.vyv.hov2.repository;


import com.vyv.hov2.domain.BaseEntity;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.io.Serializable;

/**
 * The type Custom repository.
 *
 * @param <T>  the type parameter
 * @param <ID> the type parameter
 */
@Transactional public class CustomRepositoryImpl<T extends BaseEntity, ID extends Serializable> extends SimpleJpaRepository<T, ID>
        implements CustomRepository<T, ID> {

    /**
     * The Entity manager.
     */
    private final EntityManager entityManager;
    /**
     * The Logger.
     */
    private static final Log logger = LogFactory.getLog(CustomRepositoryImpl.class);

    /**
     * Instantiates a new Custom repository.
     *
     * @param entityInformation the entity information
     * @param entityManager     the entity manager
     */
    CustomRepositoryImpl(JpaEntityInformation<T, ?> entityInformation, EntityManager entityManager) {
        super(entityInformation, entityManager);

        // Keep the EntityManager around to used from the newly introduced methods.
        this.entityManager = entityManager;
    }

    @Override
    @Transactional
    public T findOne(ID primaryKey) {
        // all this dirty thing is that getOne is lazyloaded so we don't really know if we have it or not
        // it is just a blanck object with the id passed, and once we act on it we may get entity does not exist
        // but what i want is to know right away if the object exists or not, so had to do the
        // .get bellow on the findById and if it fails i return null, so the business understand what it means.
        if (primaryKey != null) {
            try {
                return super.findById(primaryKey).get();
            } catch (Exception ex) {
                return null;
            }
        }
        return null;
    }


    /**
     * detach.
     *
     * @param klass the klass
     */
    @Override
    public void detach(T klass) {
        entityManager.detach(klass);
    }

    /**
     * flush.
     *
     */
    @Override
    public void flush() {
        entityManager.flush();
    }
}
