package com.vyv.hov2.repository;

import com.vyv.hov2.domain.Usuario;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * The interface Usuario repository.
 */
@Repository @Transactional public interface UsuarioRepository extends CustomRepository<Usuario, Long> {
    /**
     * Find all by roldb order by nombre asc list.
     *
     * @param rolDB the rol db
     * @return the list
     */
    List<Usuario> findAllByRoldbOrderByNombreAsc(String rolDB);

    /**
     * Find by login containing ignore case list.
     *
     * @param login the login
     * @return the list
     */
    List<Usuario> findByLoginContainingIgnoreCase(String login);

    /**
     * Find by login containing ignore case and roldb list.
     *
     * @param login the login
     * @param rolDB the rol db
     * @return the list
     */
    List<Usuario> findByLoginContainingIgnoreCaseAndRoldb(String login, String rolDB);

    /**
     * Find by email containing ignore case list.
     *
     * @param email the email
     * @return the list
     */
    List<Usuario> findByEmailContainingIgnoreCase(String email);

    /**
     * Find first by email ignore case usuario.
     *
     * @param email the email
     * @return the usuario
     */
    Usuario findFirstByEmailIgnoreCase(String email);

    /**
     * Find first by login ignore case usuario.
     *
     * @param login the login
     * @return the usuario
     */
    Usuario findFirstByLoginIgnoreCase(String login);
}

