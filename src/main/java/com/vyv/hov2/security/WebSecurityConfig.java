package com.vyv.hov2.security;

import com.google.common.collect.ImmutableList;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;


/**
 * This is an example of minimal configuration for ZK + Spring Security, we open as less access as possible to run a ZK-based application.
 * Please understand the configuration and modify it upon your requirement.
 */
@Configuration @EnableWebSecurity @EnableGlobalMethodSecurity(prePostEnabled = true) public class WebSecurityConfig
        extends WebSecurityConfigurerAdapter {
    private final Log logger = LogFactory.getLog(this.getClass());
    /**
     * The Custom user details service.
     */
    @Autowired
    private com.vyv.hov2.security.CustomUserDetailsService customUserDetailsService;
    @Autowired
    private Environment env;

    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        final CorsConfiguration configuration = new CorsConfiguration();
        System.out.println("Cors is configured by corsConfigurationSource");
        configuration.setAllowedOriginPatterns(ImmutableList.of("https://*.vehiculosvip.com",
                                                                "https://192.168.50.21",
                                                                "https://localhost:4200",
                                                                "https://localhost:4000",
                                                                "http://localhost:8080",
                                                                "http://localhost:4200",
                                                                "http://localhost:4000",
                                                                "http://localhost:8080"));
        configuration.setAllowedMethods(ImmutableList.of("HEAD", "GET", "POST", "PUT", "DELETE", "PATCH"));
        configuration.setAllowCredentials(true);
        configuration.setAllowedHeaders(ImmutableList.of("Authorization", "Cache-Control", "Content-Type", "idusuario"));
        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        if (env.getProperty("server.ssl.enabled") != null &&  (
                env.getProperty("server.ssl.enabled").equals("true") || Boolean.getBoolean(env.getProperty("server.ssl.enabled")) )
        ) {
            http.cors()
                    .and().requiresChannel()
                    .anyRequest()
                    .requiresSecure();
        } else {
            http.cors()
                    .and().requiresChannel()
                    .anyRequest()
                    .requiresInsecure();
        }
        http.authorizeRequests()
                .antMatchers("/css/**", "/js/**", "/img/**", "/zkau/**", "/user/resetPassword*", "/user/changePassword*",
                             "/login*", "/web/**", "/rest/**", "/carey/**", "/Gnet/**", "/Generic/**", "/mail/**", "/tasks/**")
                .permitAll()
                .antMatchers("/user/mailPassword*").hasAuthority("CHANGE_PASSWORD_PRIVILEGE") // eso viene del
                // ejemplo de Baeldung, pero mi caso no se si sirve mucho ya que los acessos y solicitudes de cambios de PW no se hace
                // por rest
                .antMatchers("/*","/accounting/**","/admin/**","/job/**","/sales/**","/iconos/*","/user/*","/quality/*").authenticated()
                .antMatchers("/**").denyAll()
                .and().formLogin().loginPage("/login").loginProcessingUrl("/j_spring_security_check")
                .usernameParameter("j_username").passwordParameter("j_password").failureHandler(customAuthenticationFailureHandler())
                .defaultSuccessUrl("/").permitAll().and().logout().deleteCookies("JSESSIONID").logoutUrl("/logout")
                .logoutSuccessUrl("/login").and().rememberMe().key("uniqueAndSecret").userDetailsService(customUserDetailsService).and()
                .csrf().disable();
        http.headers().frameOptions().sameOrigin();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authenticationProvider());
    }

    /**
     * Authentication provider dao authentication provider.
     *
     * @return the dao authentication provider
     */
    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(customUserDetailsService);
        authProvider.setPasswordEncoder(encoder());
        return authProvider;
    }

    /**
     * Encoder password encoder.
     *
     * @return the password encoder
     */
    @Bean
    public PasswordEncoder encoder() {
        return new BCryptPasswordEncoder(12);
    }

    /**
     * Custom authentication failure handler authentication failure handler.
     *
     * @return the authentication failure handler
     */
    @Bean
    public AuthenticationFailureHandler customAuthenticationFailureHandler() {
        return new CustomAuthenticationFailureHandler();
    }
}
