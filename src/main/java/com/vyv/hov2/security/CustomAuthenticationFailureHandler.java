package com.vyv.hov2.security;

import com.vyv.hov2.domain.Usuario;
import com.vyv.hov2.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * The type Custom authentication failure handler.
 */
@Component("authenticationFailureHandler") public class CustomAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {
    /**
     * The Login attempt service.
     */
    @Autowired
    private LoginAttemptService loginAttemptService;
    /**
     * The User repository.
     */
    @Autowired
    private UsuarioRepository userRepository;


    @Override
    public void onAuthenticationFailure(final HttpServletRequest request, final HttpServletResponse response,
                                        final AuthenticationException exception) throws IOException, ServletException {
        logger.debug("entrando en onAuthenticationFailure");
        setDefaultFailureUrl("/login?login_error=1");
        super.onAuthenticationFailure(request, response, exception);
        String attempsLeft = "";
        String ipAddress = request.getHeader("X-FORWARDED-FOR");
        if (ipAddress == null) {
            ipAddress = request.getRemoteAddr();
        }
        String login = request.getParameter("j_username");
        Usuario user;
        if (login != null) {
            user = userRepository.findFirstByLoginIgnoreCase(login);
        } else {
            user = null;
        }
        if (loginAttemptService.attemptsLeftFromIP(ipAddress) != null) {
            attempsLeft = "\n Quedan " + loginAttemptService.attemptsLeftFromIP(ipAddress) + " intentos desde esa IP";
        }
        if (loginAttemptService.attemptsLeftFromUsuario(user) != null && user != null) {
            attempsLeft = attempsLeft + "\n Quedan " + loginAttemptService.attemptsLeftFromUsuario(user) + " intentos con este usuario";
        }
        String errorMessage = "Identificacion erronea." + attempsLeft;

        if (exception.getMessage().equalsIgnoreCase("User is disabled")) {
            errorMessage = "Usuario desactivado.";
        } else if (exception.getMessage().equalsIgnoreCase("User account has expired")) {
            errorMessage = "Usuario expirado.";
        } else if (exception.getMessage().equalsIgnoreCase("Ip blocked")) {
            errorMessage = "Ip bloqueada.";
        } else if (exception.getMessage().equalsIgnoreCase("User blocked")) {
            errorMessage = "Usuario bloqueado.";
        }
        logger.info(errorMessage + " ip : " + ipAddress);
        request.getSession().setAttribute(WebAttributes.AUTHENTICATION_EXCEPTION, errorMessage);
    }
}
