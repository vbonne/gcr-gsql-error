package com.vyv.hov2.security;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.vyv.hov2.business.UsuarioBusiness;
import com.vyv.hov2.domain.Usuario;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * The type Login attempt service.
 */
@Service public class LoginAttemptService {

    /**
     * The Max attempt ip.
     */
    private static final Integer MAX_ATTEMPT_IP = 10;
    /**
     * The Max attempt user.
     */
    private static final Integer MAX_ATTEMPT_USER = 3;
    /**
     * The Ban days.
     */
    private static final Integer BAN_DAYS = 1; //for Ip
    /**
     * The Ban hours.
     */
    private static final Integer BAN_HOURS = 3; //for user
    /**
     * The Attempts ip cache.
     */
    private LoadingCache<String, Integer> attemptsIPCache;
    /**
     * The Attempts user cache.
     */
    private LoadingCache<String, Integer> attemptsUserCache;
    /**
     * The constant logger.
     */
    private static final Log LOGGER = LogFactory.getLog(LoginAttemptService.class);
    /**
     * The Usuario b.
     */
    @Autowired
    UsuarioBusiness usuarioB;
    /**
     * The App context.
     */
    @Autowired
    ApplicationContext appContext;
    /**
     * Instantiates a new Login attempt service.
     */
    public LoginAttemptService() {
        super();
        attemptsIPCache = CacheBuilder.newBuilder().
                expireAfterWrite(BAN_DAYS, TimeUnit.DAYS).build(new CacheLoader<String, Integer>() {
            @Override
            public Integer load(String key) {
                return 0;
            }
        });
        attemptsUserCache = CacheBuilder.newBuilder().
                expireAfterWrite(BAN_HOURS, TimeUnit.HOURS).build(new CacheLoader<String, Integer>() {
            @Override
            public Integer load(String key) {
                return 0;
            }
        });
    }

    /**
     * Login succeeded.
     *
     * @param key the key
     */
    public void loginSucceeded(String key) {
        attemptsIPCache.invalidate(key);
        if ("AllIP".equals(key)) {
            attemptsIPCache.invalidateAll();
        }
    }

    /**
     * Login succeeded.
     *
     * @param usuario the usuario
     */
    public void loginSucceeded(Usuario usuario) {
        attemptsUserCache.invalidate(usuario.getLogin());
    }

    /**
     * Login failed.
     *
     * @param usuario the usuario
     */
    public void loginFailed(Usuario usuario) {
        int attempts = 0;
        try {
            attempts = attemptsUserCache.get(usuario.getLogin());
        } catch (ExecutionException e) {
            attempts = 0;
        }
        attempts++;
        attemptsUserCache.put(usuario.getLogin(), attempts);
    }

    /**
     * Login failed.
     *
     * @param key the key
     */
    public void loginFailed(String key) {
        int attempts = 0;
        try {
            attempts = attemptsIPCache.get(key);
        } catch (ExecutionException e) {
            attempts = 0;
        }
        attempts++;
        attemptsIPCache.put(key, attempts);
    }

    /**
     * Is blocked boolean.
     *
     * @param usuario the usuario
     * @return the boolean
     */
    public boolean isBlocked(Usuario usuario) {
        try {
            return attemptsUserCache.get(usuario.getLogin()) >= MAX_ATTEMPT_USER;
        } catch (ExecutionException e) {
            return false;
        }
    }

    /**
     * Is blocked boolean.
     *
     * @param key the key
     * @return the boolean
     */
    public boolean isBlocked(String key) {
        try {
            return attemptsIPCache.get(key) >= MAX_ATTEMPT_IP;
        } catch (ExecutionException e) {
            return false;
        }
    }

    /**
     * Attempts left from usuario integer.
     *
     * @param usuario the usuario
     * @return the integer
     */
    public Integer attemptsLeftFromUsuario(Usuario usuario) {
        try {
            if (usuario != null) {
                return MAX_ATTEMPT_USER - attemptsUserCache.get(usuario.getLogin());
            } else {
                return null;
            }
        } catch (ExecutionException e) {
            return null;
        }
    }

    /**
     * Attempts left from ip integer.
     *
     * @param ip the ip
     * @return the integer
     */
    public Integer attemptsLeftFromIP(String ip) {
        try {
            return MAX_ATTEMPT_IP - attemptsIPCache.get(ip);
        } catch (ExecutionException e) {
            return null;
        }
    }

    /**
     * Gets attempts ip cache.
     *
     * @return the attempts ip cache
     */
    public LoadingCache<String, Integer> getAttemptsIPCache() {
        return attemptsIPCache;
    }

    /**
     * Sets attempts ip cache.
     *
     * @param attemptsIPCache the attempts ip cache
     */
    public void setAttemptsIPCache(LoadingCache<String, Integer> attemptsIPCache) {
        this.attemptsIPCache = attemptsIPCache;
    }

    /**
     * Gets attempts user cache.
     *
     * @return the attempts user cache
     */
    public LoadingCache<String, Integer> getAttemptsUserCache() {
        return attemptsUserCache;
    }

    /**
     * Sets attempts user cache.
     *
     * @param attemptsUserCache the attempts user cache
     */
    public void setAttemptsUserCache(LoadingCache<String, Integer> attemptsUserCache) {
        this.attemptsUserCache = attemptsUserCache;
    }

    /**
     * Reset app user.
     *
     * @param login the login
     * @throws IOException the io exception
     */
    public void resetAppUser(String login) throws IOException {
        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpPost httpPost = new HttpPost(appContext.getEnvironment().getProperty("api.baseurl")+ "/rest/usuario/reset");
        // parametrizar
        httpPost.setHeader("Accept", "application/json");
        httpPost.setHeader("User-Agent",
                "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0" + ".3770.142 Safari/537.36");
        httpPost.setHeader("Content-Type", "application/json; utf-8");
        String token = "7ebdf001-0551-4dd3-9430-8832f6ae50f1";
        String parametros = "{\"login\": \"" + login + "\", \"token\": \"" + token + "\"}";
        StringEntity params = new StringEntity(parametros);
        LOGGER.info(parametros);
        httpPost.setEntity(params);
        HttpResponse response = httpClient.execute(httpPost);
        HttpEntity respEntity = response.getEntity();
        String content = "";
        if (respEntity != null) {
            LOGGER.info("respEntity : " + respEntity);
            String json = EntityUtils.toString(respEntity, StandardCharsets.UTF_8);
            LOGGER.info("respuesta al desbloqueo : " + json);
            JsonObject jsonObject = null ;
            if (json != null && !json.isEmpty()) jsonObject = new JsonParser().parse(json).getAsJsonObject();
            if (jsonObject != null && jsonObject.get("id") != null) {
                Long id = Long.parseLong(jsonObject.get("id").toString());
                Usuario usuario = usuarioB.buscarId(id);
                return;
            }
        } else {

            return;
        }
    }

    /**
     * Gets app intentos.
     *
     * @param tipo the tipo
     * @return the app intentos
     * @throws IOException the io exception
     */
    public Map<String, Integer> getAppIntentos(String tipo) throws IOException {
        Map<String, Integer> keys = new HashMap<>();

        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpPost httpPost;
        if ("IP".equals(tipo)) {
            httpPost = new HttpPost(appContext.getEnvironment().getProperty("api.baseurl")+ "/rest/usuario/intentosIP");
        } else {
            httpPost = new HttpPost(appContext.getEnvironment().getProperty("api.baseurl")+ "/rest/usuario/intentosUser");
        }
        httpPost.setHeader("Accept", "application/json");
        httpPost.setHeader("User-Agent",
                "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0" + ".3770.142 Safari/537.36");
        httpPost.setHeader("Content-Type", "application/json; utf-8");
        String token = "7ebdf001-0551-4dd3-9430-8832f6ae50f1";
        String parametros = "{ \"token\": \"" + token + "\"}";
        StringEntity params = new StringEntity(parametros);
        LOGGER.info(parametros);
        httpPost.setEntity(params);
        HttpResponse response = httpClient.execute(httpPost);
        HttpEntity respEntity = response.getEntity();

         /*
        keys.put("192.168.50.50",5);
        keys.put("hdiaz",30);
        Map<String, Object> map = new HashMap();
        map.put("map", keys);
        ObjectMapper mapper2 = new ObjectMapper();
        String json = mapper2.writeValueAsString(map);
        logger.info(json);
*/
        if (respEntity != null) {
            String json = EntityUtils.toString(respEntity, StandardCharsets.UTF_8);
            LOGGER.debug("tenemos la respuesta de getAppIntentos");
            LOGGER.debug(json);
            JsonObject jsonObject = null ;
            if (json != null && !json.isEmpty()) {
                jsonObject = JsonParser.parseString(json).getAsJsonObject();
            } else {
                LOGGER.debug("pero esta vacia...");
                return null;
            }
            if (jsonObject != null && jsonObject.get("map") != null) {
                ObjectMapper mapper = new ObjectMapper();
                keys = mapper.readValue(jsonObject.get("map").toString(), new TypeReference<Map<String, Integer>>() {
                });
                LOGGER.debug(keys.toString());
            } else {
                LOGGER.debug("pero no se encuentra el objeto map...");
                return null;
            }
        }
        return keys;


    }

}
