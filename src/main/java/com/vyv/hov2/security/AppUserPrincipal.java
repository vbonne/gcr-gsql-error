package com.vyv.hov2.security;


import com.vyv.hov2.domain.Usuario;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;

/**
 * The type App user principal.
 */
@Component @Scope("session") @Transactional public class AppUserPrincipal implements UserDetails {
    /**
     * The constant logger.
     */
    private static final  Logger LOGGER = Logger.getLogger(AppUserPrincipal.class.getName());
    /**
     * The User.
     */
    private final Usuario user;
    /**
     * Instantiates a new App user principal.
     *
     */
    public AppUserPrincipal() {
        this.user = new Usuario();
    }

    //
    @Override
    public String getUsername() {
        return user.getLogin();
    }

    @Override
    public String getPassword() {
        return user.getPassword();
    }

    @Override
    @Transactional
    public Collection<? extends GrantedAuthority> getAuthorities() {
        // aca crear los roles Spring basado en el usuario
        final List<GrantedAuthority> authorities = new ArrayList<>();
        switch (user.getRoldb()) {
            case "dispatch":
                authorities.add(new SimpleGrantedAuthority("DISPATCH_PRIVILEGE"));
                break;
            case "gerencia":
                authorities.add(new SimpleGrantedAuthority("GERENCIA_PRIVILEGE"));
                break;
            case "contabilidad":
                authorities.add(new SimpleGrantedAuthority("CONTABILIDAD_PRIVILEGE"));
                break;
            default:
                authorities.add(new SimpleGrantedAuthority("NO_PRIVILEGE"));
                break;
        }
        return authorities;
    }

    @Override
    public boolean isAccountNonExpired() {
        return user.isActivo();
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        if ("dispatch".equals(user.getRoldb())) return true;
        if ("gerencia".equals(user.getRoldb())) return true;
        return "contabilidad".equals(user.getRoldb());
    }

    //

    /**
     * Gets usuario.
     *
     * @return the usuario
     */
    public Usuario getUsuario() {
        return user;
    }



    /**
     * Gets granted authorities.
     *
     * @param privileges the privileges
     * @return the granted authorities
     */
    @Transactional
    public List<GrantedAuthority> getGrantedAuthorities(List<String> privileges) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        for (String privilege : privileges) {
            authorities.add(new SimpleGrantedAuthority(privilege));
        }
        return authorities;
    }


}

