package com.vyv.hov2.security;

import com.vyv.hov2.domain.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Component;

/**
 * The type Authentication success event listener.
 */
@Component public class AuthenticationSuccessEventListener implements ApplicationListener<AuthenticationSuccessEvent> {

    /**
     * The Login attempt service.
     */
    @Autowired
    private LoginAttemptService loginAttemptService;

    /**
     * event.
     *
     * @param e evento
     */
    @Override
    public void onApplicationEvent(AuthenticationSuccessEvent e) {
        WebAuthenticationDetails auth = (WebAuthenticationDetails) e.getAuthentication().getDetails();
        loginAttemptService.loginSucceeded(auth.getRemoteAddress());
        AppUserPrincipal login = (AppUserPrincipal) e.getAuthentication().getPrincipal();
        Usuario user = login.getUsuario();
        loginAttemptService.loginSucceeded(user);
    }
}
