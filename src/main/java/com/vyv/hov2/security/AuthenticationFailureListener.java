package com.vyv.hov2.security;

import com.vyv.hov2.domain.Usuario;
import com.vyv.hov2.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AuthenticationFailureBadCredentialsEvent;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * The type Authentication failure listener.
 */
@Component public class AuthenticationFailureListener implements ApplicationListener<AuthenticationFailureBadCredentialsEvent> {

    /**
     * The Login attempt service.
     */
    @Autowired
    private LoginAttemptService loginAttemptService;
    /**
     * The User repository.
     */
    @Autowired
    private UsuarioRepository userRepository;
    /**
     * The Request.
     */
    @Autowired
    private HttpServletRequest request;

    /**
     *
     * @param e evento
     */
    @Override
    public void onApplicationEvent(AuthenticationFailureBadCredentialsEvent e) {
        loginAttemptService.loginFailed(getClientIP());
        String login = (String) e.getAuthentication().getPrincipal();
        Usuario user = userRepository.findFirstByLoginIgnoreCase(login);
        if (user != null) {
            loginAttemptService.loginFailed(user);
        }
    }

    /**
     * Gets client ip.
     *
     * @return the client ip
     */
    private String getClientIP() {
        String xfHeader = request.getHeader("X-Forwarded-For");
        if (xfHeader == null) {
            return request.getRemoteAddr();
        }
        return xfHeader.split(",")[0];
    }
}
