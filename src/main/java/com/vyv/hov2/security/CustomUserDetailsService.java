package com.vyv.hov2.security;

import com.vyv.hov2.domain.Usuario;
import com.vyv.hov2.repository.UsuarioRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

/**
 * The type Custom user details service.
 */
@Service @Transactional public class CustomUserDetailsService implements UserDetailsService {

    /**
     * The User repository.
     */
    private final UsuarioRepository userRepository;
    /**
     * The Request.
     */
    private final HttpServletRequest request;
    /**
     * The Login attempt service.
     */
    private final LoginAttemptService loginAttemptService;



    /**
     * Instantiates a new Custom user details service.
     */
    public CustomUserDetailsService( UsuarioRepository userRepository, HttpServletRequest request,
                                    LoginAttemptService loginAttemptService) {
        super();
        this.userRepository = userRepository;
        this.request = request;
        this.loginAttemptService = loginAttemptService;

    }

    /**
     * Complete setup.
     */
    @PostConstruct
    public void completeSetup() {
        // userRepository = applicationContext.getBean(UsuarioRepository.class);
    }

    @Override
    public UserDetails loadUserByUsername(final String username) {
        String ip = getClientIP();
        if (loginAttemptService.isBlocked(ip)) {
            throw new RuntimeException("Ip blocked");
        }
        final Usuario appUser = userRepository.findFirstByLoginIgnoreCase(username);

        com.vyv.hov2.security.AppUserPrincipal appUserPrincipal ;

        appUserPrincipal = new com.vyv.hov2.security.AppUserPrincipal();

        if (appUser == null || "invitado".equals(appUser.getRoldb())) {

            throw new UsernameNotFoundException(username);
        } else {
            if (!appUser.isActivo()) {
                throw new RuntimeException("User is disabled");
            }
            if (loginAttemptService.isBlocked(appUser)) {
                throw new RuntimeException("User blocked");
            }
        }
        String theme = appUser.getTheme() != null ? appUser.getTheme() : "atlantic";
        //Themes.setTheme(Executions.getCurrent(), theme);
        return appUserPrincipal;
    }

    /**
     * Gets client ip.
     *
     * @return the client ip
     */
    private String getClientIP() {
        String xfHeader = request.getHeader("X-Forwarded-For");
        if (xfHeader == null) {
            return request.getRemoteAddr();
        }
        return xfHeader.split(",")[0];
    }


}
