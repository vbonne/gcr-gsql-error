package com.vyv.hov2.util.util;

/**
 * The type General exception.
 *
 * @author Vincent Bonnefon<vincent@bonnefon.com>
 */
public class GeneralException extends RuntimeException {
    /**
     * Instantiates a new General exception.
     */
    public GeneralException() {
    }

    /**
     * Instantiates a new General exception.
     *
     * @param message the message
     */
    public GeneralException(String message) {
        super(message);
    }

    /**
     * Instantiates a new General exception.
     *
     * @param cause the cause
     */
    public GeneralException(Throwable cause) {
        super(cause);
    }

    /**
     * Instantiates a new General exception.
     *
     * @param message the message
     * @param cause   the cause
     */
    public GeneralException(String message, Throwable cause) {
        super(message, cause);
    }
}
