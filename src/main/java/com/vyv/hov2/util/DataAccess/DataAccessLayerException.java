package com.vyv.hov2.util.DataAccess;

import org.springframework.dao.DataIntegrityViolationException;

/**
 * The type Data access layer exception.
 *
 * @author Vincent Bonnefon<vincent@bonnefon.com>
 */
public class DataAccessLayerException extends DataIntegrityViolationException {

    String message;
    /**
     * Instantiates a new Data access layer exception.
     *
     */
    public DataAccessLayerException(String message) {
        super(message);
    }

    /**
     * Instantiates a new Data access layer exception.
     *
     * @param cause the cause
     */
    public DataAccessLayerException(Throwable cause) {
        super(null, cause);
    }

    /**
     * Instantiates a new Data access layer exception.
     *
     * @param message the message
     * @param cause   the cause
     */
    public DataAccessLayerException(String message, Throwable cause) {
        super(message, cause);
    }
}
