package com.vyv.hov2.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
public class DataSourceConfig {
    private static final Log logger = LogFactory.getLog(DataSourceConfig.class);
    // Saving credentials in environment variables is convenient, but not secure - consider a more
    // secure solution such as https://cloud.google.com/kms/ to help keep secrets safe.
    //@Value("${spring.datasource.username}")
    private static String DB_USER = System.getenv("DB_USER");
    //@Value("${spring.datasource.password}")
    private static String DB_PASSWORD = System.getenv("DB_PASSWORD");
    //@Value("${spring.cloud.gcp.sql.database-name}")
    private static String DB_NAME  = System.getenv("DB_NAME");
    private static String DB_HOST  = System.getenv("DB_HOST");
    private static String DB_PORT  = System.getenv("DB_PORT");
    //@Value("${spring.datasource.hikari.maximum-pool-size}")
    private static String DB_MAX_POOL  = System.getenv("DB_MAX_POOL");
    //@Value("${spring.datasource.hikari.connection-timeout}")
    private static String DB_CONN_TIMEOUT  = System.getenv("DB_CONN_TIMEOUT");
    private static String IDLE_TIMEOUT  = System.getenv("IDLE_TIMEOUT");
    private static String MAX_LIFE_TIME  = System.getenv("MAX_LIFE_TIME");
    //@Value("${spring.cloud.gcp.sql.enabled}")
    private static String GOOGLE_SQL_PRIVATE_ACCESS  = System.getenv("GOOGLE_SQL_PRIVATE_ACCESS");
    //@Value("${spring.cloud.gcp.sql.instance-connection-name}")
    private static String CLOUD_SQL_CONNECTION_NAME = System.getenv("CLOUD_SQL_CONNECTION_NAME");
    private static String DIRECT_JDBC_URL = System.getenv("DIRECT_JDBC_URL");
    @Value("${spring.profiles.active}")
    private String currentActiveEnv;

    @Bean
    public DataSource dataSource() throws Exception {
        logger.info("Configuring datasource for " + currentActiveEnv);
        HikariConfig config = new HikariConfig();
        // [START cloud_sql_mysql_servlet_limit]
        // maximumPoolSize limits the total number of concurrent connections this pool will keep. Ideal
        // values for this setting are highly variable on app design, infrastructure, and database.
        if ( DB_MAX_POOL == null || DB_MAX_POOL.isEmpty()) {
            logger.info("DB_MAX_POOL was absent, configuring with default value");
            DB_MAX_POOL = "5";
        }
        logger.debug("DB_MAX_POOL " + DB_MAX_POOL);
        config.setMaximumPoolSize(Integer.parseInt(DB_MAX_POOL));
        // minimumIdle is the minimum number of idle connections Hikari maintains in the pool.
        // Additional connections will be established to meet this value unless the pool is full.
        config.setMinimumIdle(5);
        // [END cloud_sql_mysql_servlet_limit]

        // [START cloud_sql_mysql_servlet_timeout]
        // setConnectionTimeout is the maximum number of milliseconds to wait for a connection checkout.
        // Any attempt to retrieve a connection from this pool that exceeds the set limit will throw an
        // SQLException.
        if ( DB_CONN_TIMEOUT == null || DB_CONN_TIMEOUT.isEmpty()) {
            logger.info("DB_CONN_TIMEOUT was absent, configuring with default value");
            DB_CONN_TIMEOUT = "10000";// 10 seconds
        }
        logger.debug("DB_CONN_TIMEOUT " + DB_CONN_TIMEOUT);
        config.setConnectionTimeout(Integer.parseInt(DB_CONN_TIMEOUT));;
        if ( DB_USER == null || DB_USER.isEmpty()) {
            logger.info("DB_USER was absent, configuring with default value");
            DB_USER = "postgres";
        }
        if ( DB_PASSWORD == null || DB_PASSWORD.isEmpty()) {
            logger.info("DB_PASS was absent, configuring with default value");
            DB_PASSWORD = "postgres";
        }
        if ( GOOGLE_SQL_PRIVATE_ACCESS == null || GOOGLE_SQL_PRIVATE_ACCESS.isEmpty()) {
            logger.info("GOOGLE_SQL_PRIVATE_ACCESS was absent, configuring with default value");
            GOOGLE_SQL_PRIVATE_ACCESS = "false";
        }
        config.setUsername(DB_USER);
        config.setPassword(DB_PASSWORD);
        logger.info("GOOGLE_SQL_PRIVATE_ACCESS " + GOOGLE_SQL_PRIVATE_ACCESS);
        if ( !"true".equals(GOOGLE_SQL_PRIVATE_ACCESS)) {
            if ( DB_HOST == null || DB_HOST.isEmpty()) {
                logger.info("DB_HOST was absent, configuring with default value");
                throw new Exception("Cannot create DB connection without DB_HOST");
            }
            if ( DB_PORT == null || DB_PORT.isEmpty()) {
                logger.info("DB_PORT was absent, configuring with default value");
                DB_PORT = "15432";
            }
            if ( DB_NAME == null || DB_NAME.isEmpty()) {
                logger.info("DB_NAME was absent, configuring with default value");
                throw new Exception("Cannot create DB connection without DB_NAME");
            }
            logger.info("Configuring datasource with direct url");
            logger.debug("jdbc:postgresql://"+DB_HOST+":"+DB_PORT+"/"+DB_NAME);
            config.setJdbcUrl("jdbc:postgresql://"+DB_HOST+":"+DB_PORT+"/"+DB_NAME);
            config.setDriverClassName("org.postgresql.Driver");
            HikariDataSource  datasource = new HikariDataSource(config);;
            logger.info("Datasource configured with direct url");
            return datasource;
        } else {
            logger.info("Configuring datasource with google sql");
            // *************
            // [START cloud_sql_mysql_servlet_create]
            // Note: For Java users, the Cloud SQL JDBC Socket Factory can provide authenticated connections
            // which is preferred to using the Cloud SQL Proxy with Unix sockets.
            // See https://github.com/GoogleCloudPlatform/cloud-sql-jdbc-socket-factory for details.


            // The following URL is equivalent to setting the config options below:
            // jdbc:mysql:///<DB_NAME>?cloudSqlInstance=<CLOUD_SQL_CONNECTION_NAME>&
            // socketFactory=com.google.cloud.sql.mysql.SocketFactory&user=<DB_USER>&password=<DB_PASS>
            // See the link below for more info on building a JDBC URL for the Cloud SQL JDBC Socket Factory
            // https://github.com/GoogleCloudPlatform/cloud-sql-jdbc-socket-factory#creating-the-jdbc-url

            // Configure which instance and what database user to connect with.
            if ( DB_NAME == null || DB_NAME.isEmpty()) {
                logger.info("DB_NAME was absent, configuring with default value");
                throw new Exception("Cannot create DB connection without DB_NAME");
            }
            if (DIRECT_JDBC_URL != null && "false".equals(DIRECT_JDBC_URL)) {
                config.setJdbcUrl(String.format("jdbc:postgresql:///%s", DB_NAME));
                config.addDataSourceProperty("socketFactory", "com.google.cloud.sql.postgres.SocketFactory");
                if (CLOUD_SQL_CONNECTION_NAME == null || CLOUD_SQL_CONNECTION_NAME.isEmpty()) {
                    logger.error("CLOUD_SQL_CONNECTION_NAME was absent, configuring with default value");
                    throw new Exception("Cannot create DB connection without CLOUD_SQL_CONNECTION_NAME");
                }
                config.addDataSourceProperty("cloudSqlInstance", CLOUD_SQL_CONNECTION_NAME);

                // The ipTypes argument can be used to specify a comma delimited list of preferred IP types
                // for connecting to a Cloud SQL instance. The argument ipTypes=PRIVATE will force the
                // SocketFactory to connect with an instance's associated private IP.
                config.addDataSourceProperty("ipTypes", "PUBLIC,PRIVATE");
            } else {
                String url = "jdbc:postgresql:///"+DB_NAME+"?cloudSqlInstance="+CLOUD_SQL_CONNECTION_NAME
                             +"&socketFactory=com.google.cloud.sql.postgres.SocketFactory&user="+DB_USER+"&password="+DB_PASSWORD;
                logger.info("url " + url);
                config.setJdbcUrl(url);
                config.setDriverClassName("org.postgresql.Driver");
            }
            // ... Specify additional connection properties here.
            // [START_EXCLUDE]
            // idleTimeout is the maximum amount of time a connection can sit in the pool. Connections that
            // sit idle for this many milliseconds are retried if minimumIdle is exceeded.
            if ( IDLE_TIMEOUT == null || IDLE_TIMEOUT.isEmpty()) {
                logger.info("IDLE_TIMEOUT was absent, configuring with default value");
                IDLE_TIMEOUT = "600000"; // 10 minutes
            }
            logger.debug("IDLE_TIMEOUT " + IDLE_TIMEOUT);
            config.setIdleTimeout(Integer.parseInt(IDLE_TIMEOUT));
            // [END cloud_sql_mysql_servlet_timeout]

            // [START cloud_sql_mysql_servlet_backoff]
            // Hikari automatically delays between failed connection attempts, eventually reaching a
            // maximum delay of `connectionTimeout / 2` between attempts.
            // [END cloud_sql_mysql_servlet_backoff]

            // [START cloud_sql_mysql_servlet_lifetime]
            // maxLifetime is the maximum possible lifetime of a connection in the pool. Connections that
            // live longer than this many milliseconds will be closed and reestablished between uses. This
            // value should be several minutes shorter than the database's timeout value to avoid unexpected
            // terminations.
            if ( MAX_LIFE_TIME == null || MAX_LIFE_TIME.isEmpty()) {
                logger.info("MAX_LIFE_TIME was absent, configuring with default value");
                MAX_LIFE_TIME = "1800000"; // 30 minutes
            }
            logger.debug(MAX_LIFE_TIME);
            config.setMaxLifetime(Integer.parseInt(MAX_LIFE_TIME));
            // [END cloud_sql_mysql_servlet_lifetime]

            // [END_EXCLUDE]
            //config.addDataSourceProperty("useSSL", true);
            //config.addDataSourceProperty("requireSSL", true);
            //config.addDataSourceProperty("verifyServerCertificate", true);
            // Initialize the connection pool using the configuration object.
            HikariDataSource pool = new HikariDataSource(config);
            logger.info(" jdbcURL " + pool.getJdbcUrl());
            // [END cloud_sql_mysql_servlet_create]
            logger.info("Datasource configured with google sql");
            return pool;
        }
    }
}
