package com.vyv.hov2.config;

import com.vyv.hov2.repository.CustomRepositoryImpl;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * The type Jpa config.
 */
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "com.vyv.hov2.repository", repositoryBaseClass = CustomRepositoryImpl.class)
public class JpaConfig {



    /**
     * constructor.
     *
     */
    public JpaConfig() {}

    /**
     *  para tener disponible un bean con el datasource (por ejemplo para crear une bean liquibase
     *  manualmente.
     * @return DataSource
     */
    /*
    @Bean
    public  DataSource dataSource() {
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.driverClassName(env.getProperty("spring.datasource.driver-class-name"));
        dataSourceBuilder.url(env.getProperty("spring.datasource.url"));
        dataSourceBuilder.username(env.getProperty("spring.datasource.username"));
        dataSourceBuilder.password(env.getProperty("spring.datasource.password"));
        return dataSourceBuilder.build();
    }
     */
}
