package com.vyv.hov2.viewModel;


import com.vyv.hov2.business.UsuarioBusiness;
import com.vyv.hov2.business.impl.UsuarioBusinessImpl;
import com.vyv.hov2.domain.Usuario;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Div;
import org.zkoss.zul.Window;
import org.zkoss.zul.theme.Themes;

import java.io.Serializable;
import java.util.List;
import java.util.Locale;

/**
 * The type Home view model.
 *
 * @author Vincent Bonnefon<vincent@bonnefon.com>
 */
public class HomeViewModel extends AbstractViewModel implements Serializable {
    /**
     * The Div contenido.
     */
    @Wire("div#contenido")
    Div divContenido;
    /**
     * The For dispatch.
     */
    private Boolean forDispatch;
    /**
     * The For accounting.
     */
    private Boolean forAccounting;

    private Boolean forAccountingOrPagoLinea;
    /**
     * The For accounting.
     */
    private Boolean forVentas;

    private Boolean providersAccess;
    /**
     * The For quality.
     */
    private Boolean forQuality = false;
    /**
     * The For all.
     */
    private Boolean forAll;
    /**
     * The For debug.
     */
    private Boolean forDebug;
    /**
     * The For external.
     */
    private Boolean forExternal;
    /**
     * The For gerente.
     */
    private Boolean forGerente;
    /**
     * The Panel dispatch open.
     */
    private Boolean panelDispatchOpen;
    /**
     * The Panl contabilidad open.
     */
    private Boolean panlContabilidadOpen;
    /**
     * The Has security access.
     */
    private Boolean hasSecurityAccess = false;
    /**
     * The Has security config.
     */
    private Boolean hasSecurityConfig = false;

    /**
     * The Has security config.
     */
    private Boolean hasVehicleAccess = false;
    /**
     * The Has reset blocked.
     */
    private Boolean hasResetBlocked = false;
    /**
     * The Welcome.
     */
    private String welcome;


    /**
     * The Win.
     */
    @Wire("window#_home")
    Window win;


    /**
     * The Empleado.
     */
    UsuarioBusinessImpl empleado;
    /**
     * The E.
     */
    UsuarioBusiness e;


    /**
     * The Logger.
     */
    private static final Log logger = LogFactory.getLog(HomeViewModel.class);


    /**
     * After compose.
     *
     * @param view    the view
     * @param session the session
     */
    @AfterCompose
    @Transactional
    public void afterCompose(@ContextParam(ContextType.VIEW) Component view, @ContextParam(ContextType.SESSION) Session session) {
        super.afterCompose(view);
        Usuario usuario = getAppUser().getUsuario();
        if (usuario == null) {
            logger.debug("usuario es nullo");
            return;
        } else {
            if (!"conductor".equals(usuario.getRoldb()) && !"dispatch".equals(usuario.getRoldb()) && !"gerencia".equals(usuario.getRoldb())
                && !"contabilidad".equals(usuario.getRoldb())) {
                // este control en algun momento tocara quitarlo pero por el momento agregua una capa de seguridad adicional
                this.cerrarSesion();
            }
            logger.debug("parece todo bien");
            welcome = usuario.getNombre();
        }
        logger.debug("usuario.nombre " + usuario.getNombre());
        logger.debug("usuario.getRoldb " + usuario.getRoldb());
        logger.debug("usuario.getId " + usuario.getId());
        logger.debug( "session.getAttributes()" + session.getAttributes().toString());
        forAll = true;
        forExternal = true;
            }




    /**
     * Cerrar sesion.
     */
    @Command
    public void cerrarSesion() {

    }

    /**
     * Goto url.
     *
     * @param url the url
     */
    @Command
    public void gotoUrl(@BindingParam("url") String url) {

    }

    /**
     * Switch theme.
     *
     * @param theme the theme
     */
    @Command
    public void switchTheme(@BindingParam("theme") String theme) {
        for (String t : Themes.getThemes()) {
            logger.debug(" Entrandro en switchTheme " + t);
        }
        Themes.setTheme(Executions.getCurrent(), theme);
        for (String t : Themes.getThemes()) {
            logger.debug(" saliendo con el tema en switchTheme " + Themes.getTheme(Executions.getCurrent()));
            logger.debug(" saliendo con el tema en switchTheme " + Themes.getCurrentTheme());
        }
        Executions.sendRedirect("");
    }
    /**
     * Switch language.
     *
     * @param locale the locale
     */
    @Command
    public void switchLanguage(@BindingParam("locale") String locale) {
        Locale prefer_locale = locale.length() > 2 ?
                new Locale(locale.substring(0,2),locale.substring(3)) : new Locale(locale);
        Executions.sendRedirect("");
    }

    /**
     * Gets theme icon.
     *
     * @return the theme icon
     */
    public String getThemeIcon() {
        logger.debug("2 contra el theme activo :  " + Themes.getTheme(Executions.getCurrent()));
        return Themes.getTheme(Executions.getCurrent());
    }




    /**
     * get for dispatch.
     *
     * @return  boolean
     */
    public Boolean getForDispatch() {
        return forDispatch;
    }

    /**
     * set for dispatch.
     *
     * @param forDispatch the for dispatch
     */
    public void setForDispatch(Boolean forDispatch) {
        this.forDispatch = forDispatch;
    }

    /**
     * get for accounting.
     *
     * @return  boolean
     */
    public Boolean getForAccounting() {
        return forAccounting;
    }

    /**
     * set for accounting.
     *
     * @param forAccounting the for accounting
     */
    public void setForAccounting(Boolean forAccounting) {
        this.forAccounting = forAccounting;
    }

    /**
     * get for all.
     *
     * @return  boolean
     */
    public Boolean getForAll() {
        return forAll;
    }

    /**
     * set for all.
     *
     * @param forAll the for all
     */
    public void setForAll(Boolean forAll) {
        this.forAll = forAll;
    }

    /**
     * get for debug.
     *
     * @return  boolean
     */
    public Boolean getForDebug() {
        return forDebug;
    }

    /**
     * set for debug.
     *
     * @param forDebug the for debug
     */
    public void setForDebug(Boolean forDebug) {
        this.forDebug = forDebug;
    }

    /**
     * get for external.
     *
     * @return boolean
     */
    public Boolean getForExternal() {
        return forExternal;
    }

    /**
     * set for external.
     *
     * @param forExternal the for external
     */
    public void setForExternal(Boolean forExternal) {
        this.forExternal = forExternal;
    }

    /**
     * get for gerente.
     *
     * @return boolean
     */
    public Boolean getForGerente() {
        return forGerente;
    }

    /**
     * set for gerente.
     *
     * @param forGerente the for gerente
     */
    public void setForGerente(Boolean forGerente) {
        this.forGerente = forGerente;
    }

    /**
     * Gets panel dispatch open.
     *
     * @return the panel dispatch open
     */
    public Boolean getPanelDispatchOpen() {
        return panelDispatchOpen;
    }

    /**
     * Sets panel dispatch open.
     *
     * @param panelDispatchOpen the panel dispatch open
     */
    public void setPanelDispatchOpen(Boolean panelDispatchOpen) {
        this.panelDispatchOpen = panelDispatchOpen;
    }

    /**
     * Gets panl contabilidad open.
     *
     * @return the panl contabilidad open
     */
    public Boolean getPanlContabilidadOpen() {
        return panlContabilidadOpen;
    }

    /**
     * Sets panl contabilidad open.
     *
     * @param panlContabilidadOpen the panl contabilidad open
     */
    public void setPanlContabilidadOpen(Boolean panlContabilidadOpen) {
        this.panlContabilidadOpen = panlContabilidadOpen;
    }

    /**
     * Gets welcome.
     *
     * @return the welcome
     */
    public String getWelcome() {
        return welcome;
    }

    /**
     * Sets welcome.
     *
     * @param welcome the welcome
     */
    public void setWelcome(String welcome) {
        this.welcome = welcome;
    }



    /**
     * Gets has security access.
     *
     * @return the has security access
     */
    public Boolean getHasSecurityAccess() {
        return hasSecurityAccess;
    }

    /**
     * Sets has security access.
     *
     * @param hasSecurityAccess the has security access
     */
    public void setHasSecurityAccess(Boolean hasSecurityAccess) {
        this.hasSecurityAccess = hasSecurityAccess;
    }

    /**
     * Gets has security config.
     *
     * @return the has security config
     */
    public Boolean getHasSecurityConfig() {
        return hasSecurityConfig;
    }

    /**
     * Sets has security config.
     *
     * @param hasSecurityConfig the has security config
     */
    public void setHasSecurityConfig(Boolean hasSecurityConfig) {
        this.hasSecurityConfig = hasSecurityConfig;
    }

    /**
     * Gets has reset blocked.
     *
     * @return the has reset blocked
     */
    public Boolean getHasResetBlocked() {
        return hasResetBlocked;
    }

    /**
     * Sets has reset blocked.
     *
     * @param hasResetBlocked the has reset blocked
     */
    public void setHasResetBlocked(Boolean hasResetBlocked) {
        this.hasResetBlocked = hasResetBlocked;
    }

    /**
     * Gets for quality.
     *
     * @return the for quality
     */
    public Boolean getForQuality() {
        return forQuality;
    }

    /**
     * Sets for quality.
     *
     * @param forQuality the for quality
     */
    public void setForQuality(Boolean forQuality) {
        this.forQuality = forQuality;
    }

    public Boolean getForVentas() {
        return forVentas;
    }

    public void setForVentas(Boolean forVentas) {
        this.forVentas = forVentas;
    }

    public Boolean getProvidersAccess() {
        return providersAccess;
    }

    public void setProvidersAccess(Boolean providersAccess) {
        this.providersAccess = providersAccess;
    }

    public Boolean getHasVehicleAccess() {
        return hasVehicleAccess;
    }

    public void setHasVehicleAccess(Boolean hasVehicleAccess) {
        this.hasVehicleAccess = hasVehicleAccess;
    }

    public Boolean getForAccountingOrPagoLinea() {
        return forAccountingOrPagoLinea;
    }

    public void setForAccountingOrPagoLinea(Boolean forAccountingOrPagoLinea) {
        this.forAccountingOrPagoLinea = forAccountingOrPagoLinea;
    }
}
