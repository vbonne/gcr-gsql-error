package com.vyv.hov2.viewModel;

import com.vyv.hov2.business.UsuarioBusiness;
import com.vyv.hov2.domain.Usuario;
import com.vyv.hov2.security.AppUserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.Selectors;

/**
 * The type Abstract view model.
 */
public abstract class AbstractViewModel {

    /**
     * The Usuario b.
     */
    @Autowired
    private UsuarioBusiness usuarioB;
    /**
     * The For dispatch.
     */
    private Boolean forDispatch;
    /**
     * The For accounting.
     */
    private Boolean forAccounting;
    /**
     * The For all.
     */
    private Boolean forAll;
    /**
     * The For debug.
     */
    private Boolean forDebug;
    /**
     * The For external.
     */
    private Boolean forExternal;
    /**
     * The For gerente.
     */
    private Boolean forGerente;
    /**
     * The Usuario logged.
     */
    private Usuario usuarioLogged;

    /**
     * Instantiates a new Abstract view model.
     */
    public AbstractViewModel() {
        this.autowire(this);
    }

    /**
     * The App user.
     */
    private AppUserPrincipal appUser;


    /**
     * After compose.
     *
     * @param view the view
     */
    @AfterCompose
    @Transactional
    public void afterCompose(Component view) {
        //eso es necesario porque sino el @wire no funciona, es un bug ZK
        // TODO: remover cuando los hayan arreglado en ZK
        Selectors.wireComponents(view, this, false);

        if (usuarioB.hasRole("ROL_DISPATCH")) {
            forAll = true;
            forExternal = true;
            forDispatch = true;
            forAccounting = false;
            forDebug = false;
            forGerente = false;
        } else if (usuarioB.hasRole("ROLE_GERENCIA")) {
            forAll = true;
            forExternal = true;
            forDispatch = true;
            forAccounting = true;
            forDebug = true;
            forGerente = true;
        } else if (usuarioB.hasRole("ROLE_CONTABILIDAD")) {
            forAll = true;
            forExternal = true;
            forDispatch = true;
            forAccounting = true;
            forDebug = false;
            forGerente = false;
        }
        try {
            appUser = (AppUserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            usuarioLogged = appUser.getUsuario();
        } catch (Exception ex) {
            // as este nivel si no hay usuario conectado no es grave
        }
    }

    /**
     * Autowire.
     *
     * @param object the object
     */
    protected final void autowire(Object object) {
        this.getApplicationContext().getAutowireCapableBeanFactory().autowireBean(object);
        this.getApplicationContext().getAutowireCapableBeanFactory().initializeBean(object, "");
    }

    /**
     * Gets the application context.
     * git init
     * git remote add origin git@gitlab.com:vbonne/hov2-springboot.git
     * git add .
     * git commit -m "Initial commit"
     * git push -u origin master
     *
     * @return the application context
     */
    protected final ApplicationContext getApplicationContext() {
        return WebApplicationContextUtils
                .getRequiredWebApplicationContext(Executions.getCurrent().getDesktop().getWebApp().getServletContext());
    }

    /**
     * Gets for dispatch.
     *
     * @return the for dispatch
     */
    public Boolean getForDispatch() {
        return forDispatch;
    }

    /**
     * Sets for dispatch.
     *
     * @param forDispatch the for dispatch
     */
    public void setForDispatch(Boolean forDispatch) {
        this.forDispatch = forDispatch;
    }

    /**
     * Gets for accounting.
     *
     * @return the for accounting
     */
    public Boolean getForAccounting() {
        return forAccounting;
    }

    /**
     * Sets for accounting.
     *
     * @param forAccounting the for accounting
     */
    public void setForAccounting(Boolean forAccounting) {
        this.forAccounting = forAccounting;
    }

    /**
     * Gets for all.
     *
     * @return the for all
     */
    public Boolean getForAll() {
        return forAll;
    }

    /**
     * Sets for all.
     *
     * @param forAll the for all
     */
    public void setForAll(Boolean forAll) {
        this.forAll = forAll;
    }

    /**
     * Gets for debug.
     *
     * @return the for debug
     */
    public Boolean getForDebug() {
        return forDebug;
    }

    /**
     * Sets for debug.
     *
     * @param forDebug the for debug
     */
    public void setForDebug(Boolean forDebug) {
        this.forDebug = forDebug;
    }

    /**
     * Gets for external.
     *
     * @return the for external
     */
    public Boolean getForExternal() {
        return forExternal;
    }

    /**
     * Sets for external.
     *
     * @param forExternal the for external
     */
    public void setForExternal(Boolean forExternal) {
        this.forExternal = forExternal;
    }

    /**
     * Gets for gerente.
     *
     * @return the for gerente
     */
    public Boolean getForGerente() {
        return forGerente;
    }

    /**
     * Sets for gerente.
     *
     * @param forGerente the for gerente
     */
    public void setForGerente(Boolean forGerente) {
        this.forGerente = forGerente;
    }

    /**
     * Gets app user.
     *
     * @return the app user
     */
    public AppUserPrincipal getAppUser() {
        return appUser;
    }

    /**
     * Sets app user.
     *
     * @param appUser the app user
     */
    public void setAppUser(AppUserPrincipal appUser) {
        this.appUser = appUser;
    }

    /**
     * Gets usuario logged.
     *
     * @return the usuario logged
     */
    public Usuario getUsuarioLogged() {
        return usuarioLogged;
    }

    /**
     * Sets usuario logged.
     *
     * @param usuarioLogged the usuario logged
     */
    public void setUsuarioLogged(Usuario usuarioLogged) {
        this.usuarioLogged = usuarioLogged;
    }
}
