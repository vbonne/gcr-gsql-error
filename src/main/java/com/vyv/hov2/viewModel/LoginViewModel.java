package com.vyv.hov2.viewModel;


import com.vyv.hov2.business.UsuarioBusiness;
import com.vyv.hov2.domain.Usuario;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.zk.ui.Component;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


/**
 * The type Login view model.
 */
@Controller @Scope("session") public class LoginViewModel extends AbstractViewModel implements Serializable {

    /**
     * The Logger.
     */
    private static final Log logger = LogFactory.getLog(LoginViewModel.class);
    /**
     * The Revision.
     */
    private String revision = "";
    /**
     * The Version.
     */
    private final String version = "";
    /**
     * The Vbox logged out is visible.
     */
    private Boolean vboxLoggedOutIsVisible;
    /**
     * The Div logged in is visible.
     */
    private Boolean divLoggedInIsVisible;
    /**
     * The Txt usuario.
     */
    private String txtUsuario;
    /**
     * The Txt password.
     */
    private String txtPassword;
    /**
     * The Txt rol.
     */
    private String txtRol;

    /**
     * The Commit message.
     */
    @Value("${git.commit.message.short}")
    private String commitMessage;

    /**
     * The Branch.
     */
    @Value("${git.branch}")
    private String branch;

    /**
     * The Commit id.
     */
    @Value("${git.commit.id}")
    private String commitId;

    /**
     * The Commit id.
     */
    @Value("${spring.profiles.active}")
    private String activeProfile;

    /**
     * The Commit buildTimestamp.
     */
    @Value("${app.buildTimestamp}")
    private String mavenBuildDate;

    private List<Usuario> usuarios = new ArrayList<>();
    @Autowired
    private UsuarioBusiness usuarioBusiness;

    /**
     * After compose.
     *
     * @param view the view
     */
    @AfterCompose
    @Transactional
    public void afterCompose(@ContextParam(ContextType.VIEW) Component view) {
        super.afterCompose(view);
        Locale locale = new Locale("en", "US");
        Locale.setDefault(locale);
        usuarios = usuarioBusiness.getAppUsuarios();
        branch = String.valueOf(usuarios.size());
    }

    /**
     * Gets revision.
     *
     * @return the revision
     */
    public String getRevision() {
        if (!"dev".equalsIgnoreCase(this.activeProfile)) {
            return revision;
        }
        return " Commit ID : "+ commitId + "\nCommit Message : " + commitMessage ;
    }

    /**
     * Gets version.
     *
     * @return the version
     */
    public String getVersion() {
        if (!"dev".equalsIgnoreCase(this.activeProfile)) {
            return version;
        }
        return "Branch :" + branch+"\nBuild Date : " + mavenBuildDate;
    }
    /**
     * Gets vbox logged out is visible.
     *
     * @return the vbox logged out is visible
     */
    public Boolean getVboxLoggedOutIsVisible() {
        return vboxLoggedOutIsVisible;
    }

    /**
     * Sets vbox logged out is visible.
     *
     * @param vboxLoggedOutIsVisible the vbox logged out is visible
     */
    public void setVboxLoggedOutIsVisible(Boolean vboxLoggedOutIsVisible) {
        this.vboxLoggedOutIsVisible = vboxLoggedOutIsVisible;
    }

    /**
     * Gets div logged in is visible.
     *
     * @return the div logged in is visible
     */
    public Boolean getDivLoggedInIsVisible() {
        return divLoggedInIsVisible;
    }

    /**
     * Sets div logged in is visible.
     *
     * @param divLoggedInIsVisible the div logged in is visible
     */
    public void setDivLoggedInIsVisible(Boolean divLoggedInIsVisible) {
        this.divLoggedInIsVisible = divLoggedInIsVisible;
    }

    /**
     * Gets txt usuario.
     *
     * @return the txt usuario
     */
    public String getTxtUsuario() {
        return txtUsuario;
    }

    /**
     * Sets txt usuario.
     *
     * @param txtUsuario the txt usuario
     */
    public void setTxtUsuario(String txtUsuario) {
        this.txtUsuario = txtUsuario;
    }

    /**
     * Gets txt password.
     *
     * @return the txt password
     */
    public String getTxtPassword() {
        return txtPassword;
    }

    /**
     * Sets txt password.
     *
     * @param txtPassword the txt password
     */
    public void setTxtPassword(String txtPassword) {
        this.txtPassword = txtPassword;
    }

    /**
     * Gets txt rol.
     *
     * @return the txt rol
     */
    public String getTxtRol() {
        return txtRol;
    }

    /**
     * Sets txt rol.
     *
     * @param txtRol the txt rol
     */
    public void setTxtRol(String txtRol) {
        this.txtRol = txtRol;
    }
}
