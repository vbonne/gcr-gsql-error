Steps to reproduce the error:
* create GCP a project
* create a cloudrun service in the project
* create cloud sql postgres instance in the project with one database allow access to services of the project
* download https://gitlab.com/vbonne/gcr-gsql-error project
* create docker based on this project (docker build -t gcr.io/<GCP-project-name>/gcrerror )
* upload docker image to GCP project (should have been done automatically at previous step)
* start service with uploaded image on a 1cpu 1gb with variables set as in previous post and DLL_HANDLING="update" for database tables creation and with conection activated for the sql instance
* create 1 job with cloud scheduler to a GET to https://<generated-url>.a.run.app/login in order to test sql conection every 5 minutes or less to avoid the instances automatic stop/start that would delay the appearance of the error.

* test login page manually https://.a.run.app/login you should get a login form
* wait , within max 24h max the conection with the sql instance will be lost and the https://<generated-url>.a.run.app/login will only display "ERROR".

