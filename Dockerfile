FROM maven:3.6.3-openjdk-11-slim as BUILD
MAINTAINER vincent@bonnefon.com
WORKDIR /usr/src/app
COPY pom.xml .
RUN mvn -B dependency:resolve dependency:resolve-plugins
COPY src src
RUN mvn -B -e -C -T 1C package  -Dmaven.test.skip=true
FROM openjdk:11-jre-slim
ARG TAG_FOR_GIT
EXPOSE 8081
EXPOSE 8031
EXPOSE 2083
ENV JAVA_TOOL_OPTIONS=" -Djava.library.path=/usr/local/apr/lib "
COPY --from=BUILD /usr/src/app/target/*.jar /opt/target/hov2.jar
WORKDIR /opt/target
ENV TZ="America/Bogota"
ENV SPRING_PROFILES_ACTIVE="prod"
ENV APP_HTTP_THREADS="4"
ENV DB_MAX_POOL="4"
ENV DB_CONN_TIMEOUT="20000"
ENV DB_HOST="192.168.50.21"
ENV DB_PORT="35432"
ENV DB_NAME="vehiculosvip_2"
ENV DB_USER="postgres"
ENV DB_PASSWORD="PostGr3s+-.."
# p98Bi2ucqxI1hp5j
ENV SERVER_PORT="8031"
ENV IDLE_TIMEOUT="600000"
ENV MAX_LIFE_TIME="1800000"
ENV GIT_TAG=$TAG_FOR_GIT
ENV LOG_LEVEL="DEBUG"
ENV MAIL_LOG_LEVEL="ERROR"
ENV CRON_ENABLED=""
ENV DLL_HANDLING="update"
RUN apt-get -y update && apt-get -y install apt-utils wget tzdata ttf-dejavu ttf-dejavu-extra libfreetype6  libfontconfig1 libtcnative-1 && apt-get -y clean && mkdir bin && ln -s /usr/lib/x86_64-linux-gnu/libtcnative-1.so /opt/target/bin/libtcnative-1.so
ENTRYPOINT exec java -Djava.security.egd=file:/dev/./urandom -jar hov2.jar $JAR_OPTS $JAVA_TOOL_OPTIONS
